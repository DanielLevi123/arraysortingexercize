﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonHomework
{
    class Person : IComparable <Person>
    {
        public int Id { get; private set; }
        public int Age { get; private set; }
        public float Height { get; private set; }
        public string Name { get; private set; }

        public Person(int id, int age, float height, string name)
        {
            Id = id;
            Age = age;
            Height = height;
            Name = name;
        }
        public int CompareTo(Person p)
        {
            return this.Id - p.Id;
        }

        public override string ToString()
        {
            return $"[{base.ToString()}] Name: {Name} ID:{Id} Height: {Height} Age:{Age}";
        }
    }
}
