﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonHomework
{
    class Program
    {
        static void PrintPersonArray(Person[] p)
        {
            foreach(Person person in p)
            {
                Console.WriteLine(person.ToString()); 
            }
        }
        static void Main(string[] args)
        {
            Person[] people = new Person[5];
            people[0] = new Person(1, 18, 1.8f, "Dani");
            people[1] = new Person(5, 20, 1.65f, "Roni");
            people[2] = new Person(4, 30, 1.7f, "David");
            people[3] = new Person(3, 25, 1.9f, "Almog");
            people[4] = new Person(2, 34, 1.77f, "Gal");
            PrintPersonArray(people);
            Console.WriteLine("================");
            Console.WriteLine("array sort by Id");
            Console.WriteLine("================");
            Array.Sort(people);
            PrintPersonArray(people);
            Console.WriteLine("================");
            Console.WriteLine("array sort by Name");
            Console.WriteLine("================");
            PersonCompareByName personCompareByName = new PersonCompareByName();
            Array.Sort(people,personCompareByName);
            PrintPersonArray(people);
            Console.WriteLine("================");
            Console.WriteLine("array sort by Height");
            Console.WriteLine("================");
            PersonComparebyHeight personComparebyHeight = new PersonComparebyHeight();
            Array.Sort(people,personComparebyHeight);
            PrintPersonArray(people);
            Console.WriteLine("================");
            Console.WriteLine("array sort by Age");
            Console.WriteLine("================");
            PersonAgecomparer personAgecomparer = new PersonAgecomparer();
            Array.Sort(people,personAgecomparer);
            PrintPersonArray(people);
        }
    }
}
